import os
import json
import time
import random
import pandas as pd
import numpy as np
from gensim.models import FastText

import torch
from torch import nn, optim
from torch.autograd import Variable
from torch.nn import functional as F
from torch.utils.data import Dataset, DataLoader, SequentialSampler

from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split


class ProcessData(Dataset):
    def __init__(self, reviews, ratings, max_len, embed_model, dim):
        self.reviews = reviews
        self.ratings = ratings
        self.max_len = max_len
        self.embed_model = embed_model
        self.dim = dim

    def __len__(self):
        return len(self.reviews)

    def __getitem__(self, item):
        review = str(self.reviews[item])
        rating = self.ratings[item]
        encoding = create_embedding_layer(review,
                                          self.embed_model,
                                          self.max_len)
        return {'review_text': review,
                'encoding': encoding,
                'rating': torch.tensor(rating, dtype=torch.long)}


def create_data_loader(df, max_len, batch_size, embed_model, dim):
    ds = ProcessData(reviews=df.reviews_clean.to_numpy(),
                     ratings=df.overall.to_numpy(),
                     max_len=max_len,
                     embed_model=embed_model,
                     dim=dim)
    return DataLoader(ds, batch_size=batch_size)


def create_embedding_layer(sentence, embedding_dict, max_len):
    embedding_layer = np.zeros((max_len, 150))
    for i in range(min(len(sentence.split()), max_len)):
        embedding_layer[i] = embedding_dict[sentence[i]]
    return embedding_layer


class LSTMClassifier(nn.Module):
    def __init__(self, batch_size, output_size, hidden_size,
                 embedding_length, n_layers):
        super(LSTMClassifier, self).__init__()
        self.embedding_length = embedding_length
        self.batch_size = batch_size
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.n_layers = n_layers
        self.lstm = nn.LSTM(embedding_length, hidden_size)
        self.label = nn.Linear(hidden_size, output_size)

    def forward(self, word_embeddings, batch_size=None):
        """
        Get the word vectors for each word in the
        review using word embeddings.

        Args:
            input_sentence (str): input review
            batch_size (int): batch size

        Return:
            final_output: output
        """
        # change the word_embeddings size from
        # (batch_size, max_len, embedding_length)
        # to (max_len, batch_size, embedding_length)
        word_embeddings = word_embeddings.permute(1, 0, 2).float()
        if batch_size is None:
            h0 = Variable(torch.zeros(1, self.batch_size, self.hidden_size).cuda())  # Initial hidden state of the LSTM
            c0 = Variable(torch.zeros(1, self.batch_size, self.hidden_size).cuda())  # Initial cell state of the LSTM
        else:
            h0 = Variable(torch.zeros(1, batch_size, self.hidden_size).cuda())
            c0 = Variable(torch.zeros(1, batch_size, self.hidden_size).cuda())
        output, (final_hidden_state, final_cell_state) = self.lstm(word_embeddings, (h0, c0))
        # output = output[:, -1, :]
        final_output = self.label(final_hidden_state[-1])
        return final_output


def clip_gradient(model, clip_value):
    # to prevent gradient exploding problem
    params = list(filter(lambda p: p.grad is not None, model.parameters()))
    for p in params:
        p.grad.data.clamp_(-clip_value, clip_value)


def train_model(model, train_iter, epoch):
    total_epoch_loss = 0
    total_epoch_acc = 0
    model.cuda()
    # adam's method considered as a method of Stochastic Optimization is a technique implementing adaptive learning rate (rephrase)
    optim = torch.optim.Adam(filter(lambda p: p.requires_grad, model.parameters()))  # want the model to get trained to reach the state of maximum accuracy given constraint like time, computing power and memory (rephrase)
    steps = 0
    model.train()
    for idx, batch in enumerate(train_iter):
        encoding = batch['encoding'].to(device)
        rating = batch['rating']
        rating = torch.autograd.Variable(rating).long().to(device)
        if (encoding.size()[0] != 32):  # One of the batch returned by BucketIterator has length different than 32.
            continue
        optim.zero_grad()  # set the gradients to zero before start to do back propagation. Accumulation of gradients happen when .backward() in called on the loss tensor.
        prediction = model(encoding)
        loss = loss_fn(prediction, rating)
        # torch.max(output, 1) is getting the maximum value from each row in the tensor.
        num_corrects = (torch.max(prediction, 1)[1].view(rating.size()).data == rating.data).float().sum()
        acc = num_corrects / encoding.size()[0] * 100
        loss.backward()
        clip_gradient(model, 1e-1)
        optim.step()
        steps += 1

        if steps % 100 == 0:
            print(
                f'Epoch: {epoch + 1}, Idx: {idx + 1}, Training Loss: {loss.item():.4f}, Training Accuracy: {acc.item(): .2f}%')

        total_epoch_loss += loss.item()
        total_epoch_acc += acc.item()

    return total_epoch_loss / len(train_iter), total_epoch_acc / len(train_iter)


def eval_model(model, val_iter):
    total_epoch_loss = 0
    total_epoch_acc = 0
    model.eval()
    with torch.no_grad():
        for idx, batch in enumerate(val_iter):
            encoding = batch['encoding'].to(device)
            rating = batch['rating']
            rating = torch.autograd.Variable(rating).long().to(device)
            if (encoding.size()[0] != 32):  # One of the batch returned by BucketIterator has length different than 32.
                continue
            prediction = model(encoding)
            loss = loss_fn(prediction, rating)
            num_corrects = (torch.max(prediction, 1)[1].view(rating.size()).data == rating.data).sum()
            acc = num_corrects / encoding.size()[0] * 100
            total_epoch_loss += loss.item()
            total_epoch_acc += acc.item()

    return total_epoch_loss / len(val_iter), total_epoch_acc / len(val_iter)


if __name__ == '__main__':
    INDIR = 'data'
    game_dir = os.path.join(INDIR, 'reviews_Video_Games_5_processed_2.json')
    with open(game_dir, 'r') as f:
        games = json.load(f)

    # Convert data from json to csv
    games = pd.DataFrame.from_dict(games)
    games['overall'] = games['overall'].astype(int) - 1

    # Load FastText embedding model
    embed_path = os.path.join('model/embed_model.model')
    embed_model = FastText.load(embed_path)

    MAX_LEN = 128
    BATCH_SIZE = 32
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    OUTPUT_SIZE = 5
    HIDDEN_SIZE = 256
    EMBEDDING_LENGTH = 150
    NUM_LAYERS = 1
    learning_rate = 2e-5

    # Instantiate LSTM classifier model
    model = LSTMClassifier(BATCH_SIZE, OUTPUT_SIZE, HIDDEN_SIZE, EMBEDDING_LENGTH, NUM_LAYERS)
    loss_fn = F.cross_entropy

    # Create data loader
    train_data, test_data = train_test_split(games, test_size=0.3, random_state=123)
    train_data_loader = create_data_loader(train_data, MAX_LEN, BATCH_SIZE, embed_model, 150)
    test_data_loader = create_data_loader(test_data, MAX_LEN, BATCH_SIZE, embed_model, 150)

    # Train lstm classifier model
    for epoch in range(1):
        train_loss, train_acc = train_model(model, train_data_loader, epoch)
        val_loss, val_acc = eval_model(model, test_data_loader)

        print(f'Epoch: {epoch + 1:02}, Train Loss: {train_loss:.3f}, Train Acc: {train_acc:.2f}%, Val. Loss: {val_loss:3f}, Val. Acc: {val_acc:.2f}%')
